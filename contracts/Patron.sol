pragma solidity ^0.4.24;

contract Patron {

       		// Defining variable holder of contract
	address holder;

		// Setting holder
	function shuffle() { holder = msg.sender; }

		// Recovering funds 	
	function kill() { if (msg.sender == holder) selfdestruct(holder); }
}

contract Loudmouth is Patron {
		// Defining variable message
	string greeting;

		// Setting message
	function Greeter(string _greeting) public {
		greeting = _greeting;
	}

		// Return message
	function greet() constant returns (string) {
		return greeting;
	}
}	
